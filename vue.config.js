const path = require('path');

module.exports = {
    chainWebpack: config => {
        config.resolve.alias
            .set('@src', path.resolve(__dirname, 'src/'))
            .set('@pages', path.resolve(__dirname, 'src/pages'))
            .set('@layouts', path.resolve(__dirname, 'src/layouts'))
            .set('@store', path.resolve(__dirname, 'src/store'))
            .set('@router', path.resolve(__dirname, 'src/router'))
            .set('@images', path.resolve(__dirname, 'src/assets/images'))
            .set('@components', path.resolve(__dirname, 'src/components'))
            .set('@unit-tests', path.resolve(__dirname, 'tests/unit'));
    },
    transpileDependencies: [
        'vuetify',
    ],
};
