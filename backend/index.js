const express = require('express');
const socketIO = require('socket.io');
const path = require('path');
const http = require('http');

const port = process.env.PORT || 3000;

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

io.on('connection', socket => {
    socket.on('userJoined', roomData => {
        socket.join(roomData.id);
        socket.emit('newAdminMessage', `Hello ${roomData.userEmail}`);
        socket.broadcast
            .to(roomData.id)
            .emit('newAdminMessage', `User ${roomData.userEmail} is online`);
    });

    socket.on('createMessage', (data, cb) => {
        io.to(data.roomId).emit('newMessage', {
            [data.messageId] : {
                message: data.message,
                userId: data.userId
            }
        });

        cb();
    });

    socket.on('leaveFromChat', (roomData) => {
        socket.to(roomData.roomId).emit('newAdminMessage', `User ${roomData.userEmail} left chat`);
    })
});

server.listen(port, () => {
    console.log(`Server has been started on port ${port}...`);
});
