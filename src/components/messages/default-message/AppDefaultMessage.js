// Imports / Utils / Constants
/* There will be imports */

// Vuex
/* There will be imports */

// Routes
/* There will be imports */

// Mixins
/* There will be imports */

// Components
/* There will be imports */

// Dynamic components
/* There will be imports */

// @vue/component
export default {
    name: 'AppDefaultMessage',
    props: {
        isOwner: {
            type: Boolean,
            required: false,
            default: false,
        },
        userName: {
            type: String,
            required: false,
            default: 'User',
        },
        message: {
            type: String,
            required: true,
        },
    },
};
