// Imports / Utils / Constants
/* There will be imports */

// Vuex
import { mapActions, mapGetters } from 'vuex';

// Routes
import { HomeRoute } from '@router/main-page-routes/home.route';

// Mixins
/* There will be imports */

// Components
import Drawer from '@layouts/drawer/Drawer.vue';
import Header from '@layouts/header/Header.vue';

// Dynamic components
/* There will be imports */

// @vue/component
export default {
    name: 'ChatLayout',
    components: {
        Drawer,
        Header,
    },
    data () {
        return {
            drawer: true,
        };
    },
    computed: {
        ...mapGetters([
            'getRoomMembers',
            'getUserId',
            'getUserEmail',
        ]),
    },
    methods: {
        ...mapActions([
            'removeMemberFromRoom',
        ]),
        toggleDrawer () {
            this.drawer = !this.drawer;
        },
        async leaveFromChat () {
            const roomId = this.$route.params.id;
            const preparedData = {
                roomId,
                userEmail: this.getUserEmail,
            };

            this.$socket.emit('leaveFromChat', preparedData);
            await this.removeMemberFromRoom({
                roomId,
                userId: this.getUserId,
            });
            await this.exitChat();
        },
        exitChat () {
            this.$router.push(HomeRoute);
        },
    },
};
