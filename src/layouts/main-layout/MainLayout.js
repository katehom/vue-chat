// Imports / Utils / Constants
/* There will be imports */

// Vuex
import { mapActions, mapGetters } from 'vuex';

// Routes
import { CreateRoomRoute } from '@router/main-page-routes/create-room.route';
import { SignUpRoute } from '@router/main-page-routes/auth-routes/sign-up.route';
import { SignInRoute } from '@router/main-page-routes/auth-routes/sign-in.route';

// Mixins
/* There will be imports */

// Components
import Drawer from '@layouts/drawer/Drawer.vue';
import Header from '@layouts/header/Header.vue';

// Dynamic components
/* There will be imports */

// @vue/component
export default {
    name: 'MainLayout',
    components: {
        Drawer,
        Header,
    },
    data () {
        return {
            drawer: true,
            menuItems: {
                logged: [
                    {
                        label: 'Create new room',
                        action: () => this.$router.push(CreateRoomRoute),
                    },
                    {
                        label: 'Sign Out',
                        action: this.signOut,
                    },
                ],
                nonLogged: [
                    {
                        label: 'Sign Up',
                        action: () => this.$router.push(SignUpRoute),
                    },
                    {
                        label: 'Sign In',
                        action: () => this.$router.push(SignInRoute),
                    },
                ],
            },
        };
    },
    computed: {
        ...mapGetters([
            'getIsLoggedIn',
        ]),
    },
    methods: {
        ...mapActions([
            'signOut',
        ]),
    },
};
