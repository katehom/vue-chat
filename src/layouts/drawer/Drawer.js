// Imports / Utils / Constants
/* There will be imports */

// Vuex
/* There will be imports */

// Routes
/* There will be imports */

// Mixins
/* There will be imports */

// Components
/* There will be imports */

// Dynamic components
/* There will be imports */

// @vue/component
export default {
    name: 'Drawer',
    props: {
        value: {
            type: Boolean,
            required: true,
        },
    },
    computed: {
        drawer: {
            get () {
                return this.value;
            },
            set (value) {
                this.$emit('input', value);
            },
        },
    },
};
