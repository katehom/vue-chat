import Vue from 'vue';
import VueSocketIO from 'vue-socket.io';
import SocketIO from 'socket.io-client';
import store from '@src/store';

const socket = new VueSocketIO(({
    debug: true,
    connection: SocketIO(process.env.VUE_APP_SOCKET_ENDPOINT, { transports: [ 'websocket' ] }),
    vuex: {
        store,
        actionPrefix: 'SOCKET_',
        mutationPrefix: 'SOCKET_',
    },
}));

Vue.use(socket);

export default socket;
