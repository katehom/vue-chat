import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
import { FIREBASE_CONFIG } from '@src/config/firebase';

firebase.initializeApp(FIREBASE_CONFIG);

const db = firebase.database();
const auth = firebase.auth();

export {
    db,
    auth,
    firebase,
};
