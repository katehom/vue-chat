import { ROOMS_MUTATION_TYPES } from '@store/rooms/mutation-types';

export default {
    [ROOMS_MUTATION_TYPES.SET_ROOMS] (state, roomsData) {
        const preparedRoomsData = [];

        for (const room in roomsData) {
            preparedRoomsData.push({
                ...roomsData[room],
                id: room,
            });
        }

        state.rooms = preparedRoomsData;
    },
    [ROOMS_MUTATION_TYPES.SET_ROOM_MEMBERS] (state, roomMembers) {
        if (!state.roomMembers) {
            state.roomMembers = {};
        }

        state.roomMembers = {
            ...state.roomMembers,
            ...roomMembers,
        };
    },
    [ROOMS_MUTATION_TYPES.REMOVE_ROOM_MEMBER] (state, userId) {
        delete state.roomMembers[userId];
    },
    [ROOMS_MUTATION_TYPES.CLEAN_ROOM_MEMBERS] (state) {
        state.roomMembers = null;
    },
};
