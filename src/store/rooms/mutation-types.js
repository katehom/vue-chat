export const ROOMS_MUTATION_TYPES = {
    SET_ROOMS: 'SET_ROOMS',
    SET_ROOM_MEMBERS: 'SET_ROOM_MEMBERS',
    REMOVE_ROOM_MEMBER: 'REMOVE_ROOM_MEMBER',
    CLEAN_ROOM_MEMBERS: 'CLEAN_ROOM_MEMBERS',
};
