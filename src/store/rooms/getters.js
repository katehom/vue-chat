export default {
    getRooms (state) {
        return state.rooms;
    },
    getRoomMembers (state) {
        return state.roomMembers;
    },
};
