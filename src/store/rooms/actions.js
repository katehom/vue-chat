import { db } from '@src/firebase';
import { ROOMS_MUTATION_TYPES } from '@store/rooms/mutation-types';
import { DB_CHILD_NAMES } from '@src/config/firebase';

export default {
    async fetchAllRooms ({ commit }) {
        try {
            const dbRef = db.ref();
            const snapshot = await dbRef.child(DB_CHILD_NAMES.ROOMS).get();

            if (snapshot.exists()) {
                commit(ROOMS_MUTATION_TYPES.SET_ROOMS, snapshot.val());
            }
        } catch (e) {
            throw e;
        }
    },
    async createNewRoom ({ getters }, roomData) {
        try {
            const newRoomKey = db.ref().child(DB_CHILD_NAMES.ROOMS).push().key;

            const updates = {};
            updates[`${DB_CHILD_NAMES.ROOMS}/${newRoomKey}`] = roomData;

            await db.ref().update(updates);

            return newRoomKey;
        } catch (e) {
            throw e;
        }
    },
    async addMemberToRoom ({ getters, commit }, roomId) {
        try {
            const roomMembersBody = {
                userEmail: getters.getUserEmail,
            };
            const updates = {};

            updates[`${DB_CHILD_NAMES.ROOM_MEMBERS}/${roomId}/${DB_CHILD_NAMES.USERS}/${getters.getUserId}`] = roomMembersBody;

            await db.ref().update(updates);

            commit(ROOMS_MUTATION_TYPES.SET_ROOM_MEMBERS, {
                [getters.getUserId]: roomMembersBody,
            });
        } catch (e) {
            throw e;
        }
    },
    async fetchRoomMembers ({ commit }, roomId) {
        try {
            const roomMembers = await db.ref().child(DB_CHILD_NAMES.ROOM_MEMBERS).child(roomId).child(DB_CHILD_NAMES.USERS).get();

            if (roomMembers.exists()) {
                commit(ROOMS_MUTATION_TYPES.SET_ROOM_MEMBERS, roomMembers.val());
            }
        } catch (e) {
            throw e;
        }
    },
    async removeMemberFromRoom ({ commit }, { roomId, userId }) {
        try {
            await db.ref().child(DB_CHILD_NAMES.ROOM_MEMBERS).child(roomId).child(DB_CHILD_NAMES.USERS).child(userId).remove(() => {
                commit(ROOMS_MUTATION_TYPES.REMOVE_ROOM_MEMBER, userId);
            });
        } catch (e) {
            throw e;
        }
    },
};
