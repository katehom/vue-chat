export default {
    getUserEmail (state) {
        return state.user.email;
    },
    getIsLoggedIn (state) {
        return !!state.user.id;
    },
    getUserId (state) {
        return state.user.id;
    },
};
