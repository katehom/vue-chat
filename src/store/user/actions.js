import { auth, firebase } from '@src/firebase';
import { USER_MUTATION_TYPES } from '@store/user/mutation-types';
import { FIREBASE_USER_TOKEN } from '@src/config';

export default {
    async registerUser (state, { email, password }) {
        try {
            await auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
            await auth.createUserWithEmailAndPassword(email, password);
        } catch (e) {
            throw e;
        }
    },
    async fetchCurrentUser ({ commit }) {
        try {
            await auth.onAuthStateChanged(user => {
                user ? localStorage.setItem(FIREBASE_USER_TOKEN, user.uid) : localStorage.removeItem(FIREBASE_USER_TOKEN);
                commit(USER_MUTATION_TYPES.SET_USER, user);
            });
        } catch (e) {
            throw e;
        }
    },
    async loginUser ({ commit }, { email, password }) {
        try {
            await auth.signInWithEmailAndPassword(email, password);
        } catch (e) {
            throw e;
        }
    },
    async signOut () {
        try {
            await auth.signOut();
        } catch (e) {
            throw e;
        }
    },
};
