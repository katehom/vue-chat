import { USER_MUTATION_TYPES } from './mutation-types.js';

export default {
    [USER_MUTATION_TYPES.SET_USER] (state, user) {
        state.user.email = user?.email || null;
        state.user.id = user?.uid || null;
    },
};
