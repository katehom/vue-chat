import { db } from '@src/firebase';
import { MESSAGES_MUTATION_TYPES } from '@store/messages/mutation-types';
import { DB_CHILD_NAMES } from '@src/config/firebase';

export default {
    async sendNewMessage (state, { roomId, message, userId }) {
        try {
            const { key } = await db.ref(DB_CHILD_NAMES.MESSAGES).child(roomId).push({ message, userId });

            return key;
        } catch (e) {
            throw e;
        }
    },
    async fetchMessagesForRoom ({ commit }, roomId) {
        try {
            const messages = await db.ref().child(DB_CHILD_NAMES.MESSAGES).child(roomId).get();

            if (messages.exists()) {
                commit(MESSAGES_MUTATION_TYPES.ADD_NEW_MESSAGE, messages.val());
            }
        } catch (e) {
            throw e;
        }
    },
};
