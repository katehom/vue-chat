export default {
    getMessages (state) {
        return state.messages;
    },
    getAdminMessages (state) {
        return state.adminMessages;
    },
};
