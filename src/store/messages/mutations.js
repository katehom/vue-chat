import { MESSAGES_MUTATION_TYPES } from './mutation-types.js';

export default {
    [MESSAGES_MUTATION_TYPES.ADD_NEW_MESSAGE] (state, message) {
        if (!state.messages) {
            state.messages = {};
        }
        state.messages = {
            ...state.messages,
            ...message,
        };
    },
    [MESSAGES_MUTATION_TYPES.ADD_ADMIN_MESSAGE] (state, messagesList) {
        if (!state.adminMessages) {
            state.adminMessages = [];
        }
        state.adminMessages.push(messagesList);
    },
    [MESSAGES_MUTATION_TYPES.CLEAN_MESSAGES] (state) {
        state.messages = null;
        state.adminMessages = null;
    },
};
