import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import rooms from './rooms';
import user from './user';
import messages from './messages';

const store = new Vuex.Store({
    modules: {
        rooms,
        user,
        messages,
    },
});

export default store;
