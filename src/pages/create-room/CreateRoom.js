// Imports / Utils / Constants
import { checkIsRequired } from '@src/utils/validation';

// Vuex
import { mapActions, mapGetters } from 'vuex';

// Routes
import { ChatRoomRoute } from '@router/chat-routes/chat-room.route';

// Mixins
/* There will be imports */

// Components
/* There will be imports */

// Dynamic components
/* There will be imports */

// @vue/component
export default {
    name: 'CreateRoom',
    data () {
        return {
            dialog: true,
            roomData: {
                roomName: null,
                roomDescription: null,
            },
        };
    },
    computed: {
        ...mapGetters([
            'getUserEmail',
        ]),
    },
    methods: {
        ...mapActions([
            'createNewRoom',
        ]),
        closeDialog () {
            this.$router.back();
        },
        async createRoom () {
            const newRoomId = await this.createNewRoom(this.roomData);
            await this.$router.push({ name: ChatRoomRoute.name, params: { id: newRoomId } });
        },
        checkIsRequired,
    },
};
