// Imports / Utils / Constants
/* There will be imports */

// Vuex
import { mapActions } from 'vuex';

// Routes
import { HomeRoute } from '@router/main-page-routes/home.route';

// Mixins
/* There will be imports */

// Components
import AuthForm from '@pages/auth/auth-form/AuthForm.vue';

// Dynamic components
/* There will be imports */

// @vue/component
export default {
    name: 'SignIn',
    components: {
        AuthForm,
    },
    methods: {
        ...mapActions([
            'loginUser',
        ]),
        async signIn (user) {
            await this.loginUser(user);
            await this.$router.push(HomeRoute);
        },
    },
};
