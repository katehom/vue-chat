// Imports / Utils / Constants
import { checkIsRequired } from '@src/utils/validation';

// Vuex
/* There will be imports */

// Routes
/* There will be imports */

// Mixins
/* There will be imports */

// Components
/* There will be imports */

// Dynamic components
/* There will be imports */

// @vue/component
export default {
    name: 'AuthForm',
    props: {
        submitText: {
            type: String,
            required: true,
        },
    },
    data () {
        return {
            user: {
                email: null,
                password: null,
            },
        };
    },
    methods: {
        submitClick () {
            this.$emit('click', this.user);
        },
        checkIsRequired,
    },
};
