// Imports / Utils / Constants
/* There will be imports */

// Vuex
import { mapActions, mapGetters } from 'vuex';

// Routes
/* There will be imports */

// Mixins
/* There will be imports */

// Components
import RoomTable from '@pages/home/room-table/RoomTable.vue';

// Dynamic components
/* There will be imports */

// @vue/component
export default {
    name: 'Home',
    components: {
        RoomTable,
    },
    data () {
        return {
            headers: [
                { text: 'Room name', value: 'roomName' },
                { text: 'Description', value: 'roomDescription' },
                { text: 'Creator', value: 'userName' },
            ],
            counter: 0,
        };
    },
    computed: {
        ...mapGetters([
            'getRooms',
        ]),
    },
    async created () {
        await this.fetchAllRooms();
    },
    methods: {
        ...mapActions([
            'fetchAllRooms',
        ]),
    },
};
