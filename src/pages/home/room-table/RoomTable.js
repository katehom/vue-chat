// Imports / Utils / Constants
/* There will be imports */

// Vuex
/* There will be imports */

// Routes
import { ChatRoomRoute } from '@router/chat-routes/chat-room.route';

// Mixins
/* There will be imports */

// Components
/* There will be imports */

// Dynamic components
/* There will be imports */

// @vue/component
export default {
    name: 'RoomTable',
    props: {
        rooms: {
            type: Array,
            required: true,
        },
        headers: {
            type: Array,
            required: true,
        },
    },
    data () {
        return {
            ChatRoomRoute,
        };
    },
};
