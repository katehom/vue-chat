// Imports / Utils / Constants
/* There will be imports */

// Vuex
import { mapActions, mapGetters } from 'vuex';
import { MESSAGES_MUTATION_TYPES } from '@store/messages/mutation-types';
import { ROOMS_MUTATION_TYPES } from '@store/rooms/mutation-types';

// Routes
/* There will be imports */

// Mixins
/* There will be imports */

// Components
import ChatForm from '@pages/chat-room/chat-form/ChatForm.vue';
import AppDefaultMessage from '@components/messages/default-message/AppDefaultMessage.vue';
import AppAdminMessage from '@components/messages/admin-message/AppAdminMessage.vue';

// Dynamic components
/* There will be imports */

// @vue/component
export default {
    name: 'ChatRoom',
    components: {
        AppDefaultMessage,
        AppAdminMessage,
        ChatForm,
    },
    computed: {
        ...mapGetters([
            'getUserEmail',
            'getUserId',
            'getMessages',
            'getAdminMessages',
            'getRoomMembers',
        ]),
    },
    watch: {
        getMessages (newVal) {
            if (newVal) {
                this.$nextTick(() => {
                    window.scrollTo(0, document.body.scrollHeight);
                });
            }
        },
        getUserEmail: {
            handler (newVal) {
                if (newVal) {
                    const roomData = {
                        userEmail: this.getUserEmail,
                        id: this.$route.params.id,
                    };
                    this.$socket.emit('userJoined', roomData);
                }
            },
            immediate: true,
        },
    },
    async created () {
        const roomId = this.$route.params.id;

        await this.fetchMessagesForRoom(roomId);
        await this.fetchRoomMembers(roomId);

        if (!this.getRoomMembers?.[this.getUserId]) {
            await this.addMemberToRoom(roomId);
        }
    },
    beforeDestroy () {
        this.$store.commit(MESSAGES_MUTATION_TYPES.CLEAN_MESSAGES);
        this.$store.commit(ROOMS_MUTATION_TYPES.CLEAN_ROOM_MEMBERS);
    },
    methods: {
        ...mapActions([
            'fetchMessagesForRoom',
            'fetchRoomMembers',
            'addMemberToRoom',
        ]),
        checkIsUserOwner (userId) {
            return this.getUserId === userId;
        },
    },
};
