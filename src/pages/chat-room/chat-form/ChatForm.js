// Imports / Utils / Constants
/* There will be imports */

// Vuex
import { mapActions, mapGetters } from 'vuex';

// Routes
/* There will be imports */

// Mixins
/* There will be imports */

// Components
/* There will be imports */

// Dynamic components
/* There will be imports */

// @vue/component
export default {
    name: 'ChatForm',
    data () {
        return {
            message: null,
        };
    },
    computed: {
        ...mapGetters([
            'getUserId',
        ]),
    },
    mounted () {
        this.$refs['chat-form'].$refs.input.focus();
    },
    methods: {
        ...mapActions([
            'sendNewMessage',
        ]),
        async sendMessage () {
            if (this.message) {
                const preparedRoomData = {
                    roomId: this.$route.params.id,
                    message: this.message,
                    userId: this.getUserId,
                };
                const key = await this.sendNewMessage(preparedRoomData);

                this.$socket.emit('createMessage', {
                    ...preparedRoomData,
                    messageId: key,
                }, () => {
                    this.message = null;
                });
            }
        },
    },
};
