import ChatLayout from '@layouts/chat-layout/ChatLayout.vue';
import { ChatRoomRoute } from '@router/chat-routes/chat-room.route';

export const ChatRoutes = {
    name: 'Chat Page',
    path: '/chat/:id',
    component: ChatLayout,
    children: [
        ChatRoomRoute,
    ],
};
