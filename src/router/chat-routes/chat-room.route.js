import ChatRoom from '@pages/chat-room/ChatRoom.vue';

export const ChatRoomRoute = {
    name: 'ChatRoom Page',
    path: '',
    component: ChatRoom,
    meta: {
        requiredAuth: true,
    },
};
