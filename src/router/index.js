import Vue from 'vue';
import VueRouter from 'vue-router';

import routes from './routes';
import { userPermissionGuard } from '@router/guards/user-permission-guard';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

router.beforeEach(userPermissionGuard);

export default router;
