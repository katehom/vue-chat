import Home from '@pages/home/Home.vue';
import { CreateRoomRoute } from '@src/router/main-page-routes/create-room.route';

export const HomeRoute = {
    name: 'Home Page',
    path: '',
    component: Home,
    children: [
        CreateRoomRoute,
    ],
};
