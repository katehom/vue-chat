import CreateRoom from '@pages/create-room/CreateRoom.vue';

export const CreateRoomRoute = {
    name: 'CreateRoom Page',
    path: '/create',
    component: CreateRoom,
    meta: {
        requiredAuth: true,
    },
};
