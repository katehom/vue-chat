import MainLayout from '@layouts/main-layout/MainLayout.vue';
import { HomeRoute } from './home.route';
import { SignUpRoute } from '@router/main-page-routes/auth-routes/sign-up.route';
import { SignInRoute } from '@router/main-page-routes/auth-routes/sign-in.route';

export const MainPageChildrenRoutes = [
    HomeRoute,
    SignUpRoute,
    SignInRoute,
];

export const MainPageRoutes = {
    path: '/',
    component: MainLayout,
    children: MainPageChildrenRoutes,
};
