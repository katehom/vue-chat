import SignIn from '@pages/auth/sign-in/SignIn.vue';

export const SignInRoute = {
    name: 'SignIn Page',
    path: '/sign-in',
    component: SignIn,
    meta: {
        ifGuest: true,
    },
};
