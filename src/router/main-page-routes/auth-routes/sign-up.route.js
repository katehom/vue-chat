import SignUp from '@pages/auth/sign-up/SignUp.vue';

export const SignUpRoute = {
    name: 'SignUp Page',
    path: '/sign-up',
    component: SignUp,
    meta: {
        ifGuest: true,
    },
};
