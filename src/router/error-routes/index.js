const { PageNotFoundRoute } = require('./not-found.route');

export const ErrorPageRoutes = [
    PageNotFoundRoute,
];
