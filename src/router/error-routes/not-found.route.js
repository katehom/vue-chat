const ErrorPageNotFound = () => import('@pages/error-pages/error-404/Error404.vue');

export const PageNotFoundRoute = {
    name: 'PageNotFoundRoute',
    path: '*',
    component: ErrorPageNotFound,
};
