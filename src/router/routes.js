import { MainPageRoutes } from './main-page-routes';
import { ErrorPageRoutes } from './error-routes';
import { ChatRoutes } from '@router/chat-routes';

const routes = [
    MainPageRoutes,
    ...ErrorPageRoutes,
    ChatRoutes,
];

export default routes;
