import { FIREBASE_USER_TOKEN } from '@src/config';
import { SignInRoute } from '@router/main-page-routes/auth-routes/sign-in.route';
import { HomeRoute } from '@router/main-page-routes/home.route';

export function userPermissionGuard (to, from, next) {
    if (to.matched.some(record => record.meta.requiredAuth)) {
        if (!localStorage.getItem(FIREBASE_USER_TOKEN)) {
            next(SignInRoute);
        } else {
            next();
        }
    } else if (to.matched.some(record => record.meta.ifGuest)) {
        if (!localStorage.getItem(FIREBASE_USER_TOKEN)) {
            next();
        } else {
            next(HomeRoute);
        }
    } else {
        next();
    }
}
