import { shallowMount } from '@vue/test-utils';
import AuthForm from '@pages/auth/auth-form/AuthForm.vue';
import SignUp from '@pages/auth/sign-up/SignUp.vue';
import { mockFabric } from '@unit-tests/fabrics';

describe('SignUp.vue', () => {
    const mountFunction = options => {
        return shallowMount(SignUp, {
            ...options,
        });
    };

    it('should mount AuthForm component', () => {
        const wrapper = mountFunction();

        expect(wrapper.findComponent(AuthForm).exists()).toBeTruthy();
    });

    it('should make sign up and redirect on click', async () => {
        const user = {
            name: 'sdklsd',
        };
        const { mockStore, mockRouter } = mockFabric();
        const wrapper = mountFunction({
            mocks: {
                $store: mockStore,
                $router: mockRouter,
            },
        });

        await wrapper.findComponent(AuthForm).vm.$emit('click', user);

        expect(mockStore.dispatch).toHaveBeenLastCalledWith('registerUser', user);
        expect(mockRouter.push).toHaveBeenCalledTimes(1);
    });
});
