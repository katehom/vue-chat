import { shallowMount } from '@vue/test-utils';
import SignIn from '@pages/auth/sign-in/SignIn.vue';
import AuthForm from '@pages/auth/auth-form/AuthForm.vue';
import { mockFabric } from '@unit-tests/fabrics';

describe('SignIn.vue', () => {
    const mountFunction = options => {
        return shallowMount(SignIn, {
            ...options,
        });
    };

    it('should mount AuthForm component', () => {
        const wrapper = mountFunction();

        expect(wrapper.findComponent(AuthForm).exists()).toBeTruthy();
    });

    it('should make sign in and redirect on click', async () => {
        const user = {
            name: 'user name',
        };
        const { mockStore, mockRouter } = mockFabric();
        const wrapper = mountFunction({
            mocks: {
                $store: mockStore,
                $router: mockRouter,
            },
        });

        await wrapper.findComponent(AuthForm).vm.$emit('click', user);

        expect(mockStore.dispatch).toHaveBeenLastCalledWith('loginUser', user);
        expect(mockRouter.push).toHaveBeenCalledTimes(1);
    });
});
