import AuthForm from '@pages/auth/auth-form/AuthForm.vue';
import { createWrapperVuetify } from '@unit-tests/fabrics';

describe('AuthForm.vue', () => {
    const submitText = 'Sign In';
    const user = {
        email: 'kate@gmail.com',
        password: 'test',
    };
    const mountFunction = options => {
        return createWrapperVuetify(AuthForm, {
            propsData: {
                submitText,
                ...options,
            },
        });
    };

    it('should render props.submitText when passed', () => {
        const wrapper = mountFunction();
        const messageEl = wrapper.find('.auth-form__btn');

        expect(messageEl.text()).toBe(submitText);
    });

    it('should emit an event with user data when the submit btn is clicked', async () => {
        const wrapper = mountFunction();
        const button = wrapper.find('.auth-form__btn');
        const eventName = 'click';

        await wrapper.setData({ user });
        await button.trigger(eventName);

        expect(wrapper.emitted(eventName)[0]).toEqual([ user ]);
    });

    it('should render user.email in email input field', async () => {
        const wrapper = mountFunction();
        const userNameInput = wrapper.find('[data-username]');

        await userNameInput.setValue(user.email);

        expect(userNameInput.element.value).toEqual(user.email);
    });

    it('should render user.password in password field', async () => {
        const wrapper = mountFunction();
        const userNameInput = wrapper.find('[data-password]');

        await userNameInput.setValue(user.password);

        expect(userNameInput.element.value).toEqual(user.password);
    });
});
