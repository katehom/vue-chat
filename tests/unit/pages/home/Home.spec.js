import { shallowMount } from '@vue/test-utils';
import Home from '@pages/home/Home.vue';
import RoomTable from '@pages/home/room-table/RoomTable.vue';

describe('Home.vue', () => {
    const mockStore = { dispatch: jest.fn() };
    const mountFunction = options => {
        return shallowMount(Home, {
            mocks: {
                $store: mockStore,
            },
            stubs: [ 'router-view' ],
            computed: {
                getRooms: () => null,
            },
            ...options,
        });
    };

    it('should fetchAllRooms on created', () => {
        const fetchRoomsSpy = jest.spyOn(Home.methods, 'fetchAllRooms');

        mountFunction();

        expect(fetchRoomsSpy).toHaveBeenCalled();
    });

    it('shouldnt render RoomTable if getRooms getter is null', () => {
        const wrapper = mountFunction();

        expect(wrapper.findComponent(RoomTable).exists()).toBeFalsy();
    });

    it('should render RoomTable if getRooms getter exist', () => {
        const wrapper = mountFunction({
            computed: {
                getRooms: () => [
                    {
                        roomDescription: 'fdgnfhnfh',
                        roomName: 'dbdgbfg',
                        id: 'id',
                    },
                ],
            },
        });

        expect(wrapper.findComponent(RoomTable).exists()).toBeTruthy();
    });
});
