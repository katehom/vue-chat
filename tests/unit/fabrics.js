import { createLocalVue, mount } from '@vue/test-utils';
import Vuetify from 'vuetify';

const createTestVue = () => {
    const localVue = createLocalVue();
    const vuetify = new Vuetify();

    return { vuetify, localVue };
};

export const createWrapperVuetify = (component, options = {}) => {
    const { vuetify, localVue } = createTestVue();

    return mount(component, {
        vuetify,
        localVue,
        ...options,
    });
};

export const mockFabric = () => {
    const mockStore = {
        dispatch: jest.fn(),
    };
    const mockRouter = {
        push: jest.fn(),
    };
    const mockSocket = {
        emit: jest.fn(),
    };
    const mockRoute = {
        params: {
            id: 'roomId',
        },
    };

    return {
        mockStore,
        mockRouter,
        mockSocket,
        mockRoute,
    };
};
