import { mount } from '@vue/test-utils';
import AppAdminMessage from '@components/messages/admin-message/AppAdminMessage.vue';

describe('AppAdminMessage.vue', () => {
    const message = 'Hello';

    const mountFunction = options => {
        return mount(AppAdminMessage, {
            propsData: {
                message,
                ...options,
            },
        });
    };

    it('should render props.message when passed', () => {
        const wrapper = mountFunction();
        const messageEl = wrapper.get('.admin-message__elem');

        expect(messageEl.text()).toBe(message);
    });
});
