import { mount } from '@vue/test-utils';
import AppDefaultMessage from '@components/messages/default-message/AppDefaultMessage.vue';

describe('AppDefaultMessage.vue', () => {
    const message = 'Hello';
    const userName = 'John';

    const mountFunction = options => {
        return mount(AppDefaultMessage, {
            propsData: {
                message,
                ...options,
            },
        });
    };

    it('should render props.message when passed', () => {
        const wrapper = mountFunction();
        const messageEl = wrapper.get('.default-message__elem');

        expect(messageEl.text()).toBe(message);
    });

    it('should render props.userName when passed', () => {
        const wrapper = mountFunction({ userName });
        const messageEl = wrapper.get('.default-message__name');

        expect(messageEl.text()).toBe(userName);
    });

    it('use default value if props.userName not passed', () => {
        const defaultUserName = 'User';
        const wrapper = mountFunction();
        const messageEl = wrapper.get('.default-message__name');

        expect(messageEl.text()).toBe(defaultUserName);
    });

    it('set elem special class, if props.isOwner is passed', () => {
        const ownerClass = 'default-message--owner';
        const wrapper = mountFunction({
            isOwner: true,
        });
        const messageEl = wrapper.get('.default-message');

        expect(messageEl.classes()).toContain(ownerClass);
    });

    it('use default value, if props.isOwner is not passed', () => {
        const wrapper = mountFunction();

        expect(wrapper.props().isOwner).toBeFalsy();
    });

    it('should not set special class, if props.isOwner is not passed ', () => {
        const ownerClass = 'default-message--owner';
        const wrapper = mountFunction();
        const messageEl = wrapper.get('.default-message');

        expect(messageEl.classes()).not.toContain(ownerClass);
    });
});
