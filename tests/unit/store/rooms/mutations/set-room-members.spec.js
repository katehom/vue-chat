import { ROOMS_MUTATION_TYPES } from '@store/rooms/mutation-types';
import { ROOM_MEMBERS } from '@unit-tests/store/rooms/mocks';

import mutations from '@store/rooms/mutations';

describe('ROOMS_MUTATION_TYPES.SET_ROOM_MEMBERS', () => {
    it('should add room members to store if state.roomMembers in initial null state', () => {
        const state = {
            roomMembers: null,
        };

        mutations[ROOMS_MUTATION_TYPES.SET_ROOM_MEMBERS](state, ROOM_MEMBERS);

        expect(state.roomMembers).toEqual(ROOM_MEMBERS);
    });

    it('should add room members to store if state.roomMembers has already had members', () => {
        const ROOM_MEMBERS_INITIAL = {
            '4dIOft3': {
                userEmail: 'kate1@codeit.pro',
            },
            '4dIOft3dd': {
                userEmail: 'kate5@codeit.pro',
            },
        };
        const state = {
            roomMembers: ROOM_MEMBERS_INITIAL,
        };

        mutations[ROOMS_MUTATION_TYPES.SET_ROOM_MEMBERS](state, ROOM_MEMBERS);

        expect(state.roomMembers).toEqual({
            ...ROOM_MEMBERS_INITIAL,
            ...ROOM_MEMBERS,
        });
    });
});
