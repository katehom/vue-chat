import { ROOMS_MUTATION_TYPES } from '@store/rooms/mutation-types';
import { ROOM_MEMBER_ONE_ID, ROOM_MEMBER_TWO, ROOM_MEMBERS } from '@unit-tests/store/rooms/mocks';
import mutations from '@store/rooms/mutations';

describe('ROOMS_MUTATION_TYPES.REMOVE_ROOM_MEMBER', () => {
    it('should remove room member from state.roomMembers', () => {
        const state = {
            roomMembers: ROOM_MEMBERS,
        };

        mutations[ROOMS_MUTATION_TYPES.REMOVE_ROOM_MEMBER](state, ROOM_MEMBER_ONE_ID);

        expect(state.roomMembers).toEqual({
            ...ROOM_MEMBER_TWO,
        });
    });
});
