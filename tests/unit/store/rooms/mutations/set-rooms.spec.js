import { ROOMS_MUTATION_TYPES } from '@store/rooms/mutation-types';
import { ROOMS_INITIAL, ROOMS_PREPARED } from '@unit-tests/store/rooms/mocks';
import mutations from '@store/rooms/mutations';

describe('ROOMS_MUTATION_TYPES.SET_ROOMS', () => {
    it('should set rooms to store', () => {
        const state = {
            rooms: null,
        };

        mutations[ROOMS_MUTATION_TYPES.SET_ROOMS](state, ROOMS_INITIAL);

        expect(state).toEqual({ rooms: ROOMS_PREPARED });
    });

    it('should set new rooms to store, even if its not empty', () => {
        const state = {
            rooms: ROOMS_PREPARED,
        };

        mutations[ROOMS_MUTATION_TYPES.SET_ROOMS](state, ROOMS_INITIAL);

        expect(state).toEqual({ rooms: ROOMS_PREPARED });
    });
});
