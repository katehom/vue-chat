import { ROOM_MEMBERS } from '@unit-tests/store/rooms/mocks';
import { ROOMS_MUTATION_TYPES } from '@store/rooms/mutation-types';
import mutations from '@store/rooms/mutations';

describe('ROOMS_MUTATION_TYPES.CLEAN_ROOM_MEMBERS', () => {
    it('should clean state.roomMembers to initial null value', () => {
        const state = {
            roomMembers: ROOM_MEMBERS,
        };

        mutations[ROOMS_MUTATION_TYPES.CLEAN_ROOM_MEMBERS](state);

        expect(state.roomMembers).toBeNull();
    });
});
