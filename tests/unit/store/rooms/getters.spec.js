import getters from '@store/rooms/getters';
import { ROOM_MEMBERS, ROOMS_PREPARED } from '@unit-tests/store/rooms/mocks';

describe('getRooms getter', () => {
    const state = {
        rooms: ROOMS_PREPARED,
        roomMembers: ROOM_MEMBERS,
    };

    it('should return state.rooms', () => {
        const actual = getters.getRooms(state);

        expect(actual).toEqual(ROOMS_PREPARED);
    });

    it('should return state.roomMembers', () => {
        const actual = getters.getRoomMembers(state);

        expect(actual).toEqual(ROOM_MEMBERS);
    });
});
