export const ROOMS_INITIAL = {
    '-waiJLgnqz': {
        roomDescription: 'Room1 description',
        roomName: 'Room1',
    },
    '-aiXlfbf8': {
        roomDescription: 'Room 2 description',
        roomName: 'Room 2',
    },
};

export const ROOMS_PREPARED = [
    {
        roomDescription: 'Room1 description',
        roomName: 'Room1',
        id: '-waiJLgnqz',
    },
    {
        roomDescription: 'Room 2 description',
        roomName: 'Room 2',
        id: '-aiXlfbf8',
    },
];

export const ROOM_MEMBER_ONE_ID = '4dIOftXHLt3';
export const ROOM_MEMBER_ONE = {
    [ROOM_MEMBER_ONE_ID]: {
        userEmail: 'kate1@codeit.pro',
    },
};
export const ROOM_MEMBER_TWO = {
    '6JVjTHJBJqo': {
        userEmail: 'kate5@codeit.pro',
    },
};

export const ROOM_MEMBERS = {
    ...ROOM_MEMBER_ONE,
    ...ROOM_MEMBER_TWO,
};
