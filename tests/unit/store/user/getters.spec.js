import getters from '@store/user/getters';

const state = {
    user: {
        id: 'TofkBA1gFAMFUwXLuYp',
        email: 'kate@codeit.pro',
    },
};

describe('getUserEmail getter', () => {
    it('should return state.user.email', () => {
        const actual = getters.getUserEmail(state);

        expect(actual).toEqual(state.user.email);
    });
});

describe('getIsLoggedIn getter', () => {
    it('should return boolean from state.user.id', () => {
        const actual = getters.getIsLoggedIn(state);

        expect(actual).toEqual(!!state.user.id);
    });
});

describe('getUserId getter', () => {
    it('should return state.user.id', () => {
        const actual = getters.getUserId(state);

        expect(actual).toEqual(state.user.id);
    });
});
