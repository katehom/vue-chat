import { USER_MUTATION_TYPES } from '@store/user/mutation-types';
import { USER, USER_INITIAL } from '@unit-tests/store/user/mocks';
import mutations from '@store/user/mutations';

describe('USER_MUTATION_TYPES.SET_USER', () => {
    const state = {
        user: USER_INITIAL,
    };

    it('should set user.email/user.id if user email is exist', () => {
        mutations[USER_MUTATION_TYPES.SET_USER](state, USER);

        expect(state).toEqual({
            user: {
                email: USER.email,
                id: USER.uid,
            },
        });
    });

    it('should set user.email/user.id to initial null state if user not exist', () => {
        mutations[USER_MUTATION_TYPES.SET_USER](state, null);

        expect(state).toEqual({
            user: USER_INITIAL,
        });
    });
});
