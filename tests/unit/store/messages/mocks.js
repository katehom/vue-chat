export const MESSAGE = {
    messageId: {
        message: 'message',
        userId: 'userId',
    },
};

export const ADMIN_MESSAGE = 'Admin message';
