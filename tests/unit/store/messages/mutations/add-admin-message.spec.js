import mutations from '@store/messages/mutations';
import { MESSAGES_MUTATION_TYPES } from '@store/messages/mutation-types';
import { ADMIN_MESSAGE } from '../mocks';

describe('MESSAGES_MUTATION_TYPES.ADD_ADMIN_MESSAGE', () => {
    it('should add messages to store if state.adminMessages in initial null state', () => {
        const state = {
            adminMessages: null,
        };

        mutations[MESSAGES_MUTATION_TYPES.ADD_ADMIN_MESSAGE](state, ADMIN_MESSAGE);

        expect(state.adminMessages).toEqual([ ADMIN_MESSAGE ]);
    });

    it('should add messages to store if state.adminMessages has already had messages', () => {
        const state = {
            adminMessages: [ ADMIN_MESSAGE ],
        };

        mutations[MESSAGES_MUTATION_TYPES.ADD_ADMIN_MESSAGE](state, ADMIN_MESSAGE);

        expect(state.adminMessages).toEqual([ ADMIN_MESSAGE, ADMIN_MESSAGE ]);
    });
});
