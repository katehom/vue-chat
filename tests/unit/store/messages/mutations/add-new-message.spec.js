import mutations from '@store/messages/mutations';
import { MESSAGES_MUTATION_TYPES } from '@store/messages/mutation-types';
import { MESSAGE } from '../mocks';

describe('MESSAGES_MUTATION_TYPES.ADD_NEW_MESSAGE', () => {
    it('should add messages to store if state.messages in initial null state', () => {
        const state = {
            messages: null,
        };

        mutations[MESSAGES_MUTATION_TYPES.ADD_NEW_MESSAGE](state, MESSAGE);

        expect(state.messages).toEqual(MESSAGE);
    });

    it('should add messages to store if state.messages has already had messages', () => {
        const messageTwo = {
            messageIdTwo: {
                message: 'message',
                userId: 'userId',
            },
        };
        const state = {
            messages: {
                ...MESSAGE,
            },
        };

        mutations[MESSAGES_MUTATION_TYPES.ADD_NEW_MESSAGE](state, messageTwo);

        expect(state.messages).toEqual({
            ...MESSAGE,
            ...messageTwo,
        });
    });
});
