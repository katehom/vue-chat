import mutations from '@store/messages/mutations';
import { MESSAGES_MUTATION_TYPES } from '@store/messages/mutation-types';
import { ADMIN_MESSAGE, MESSAGE } from '../mocks';

describe('MESSAGES_MUTATION_TYPES.ADD_NEW_MESSAGE', () => {
    it('should clean all state fields', () => {
        const state = {
            messages: MESSAGE,
            adminMessages: [ ADMIN_MESSAGE ],
        };

        mutations[MESSAGES_MUTATION_TYPES.CLEAN_MESSAGES](state);

        for (const stateKey in state) {
            expect(state[stateKey]).toBeNull();
        }
    });
});
