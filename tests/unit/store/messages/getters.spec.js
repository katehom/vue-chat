import getters from '@store/messages/getters';
import { ADMIN_MESSAGE, MESSAGE } from './mocks';

describe('getMessages getter', () => {
    const state = {
        messages: MESSAGE,
    };

    it('should return state.messages', () => {
        const actual = getters.getMessages(state);

        expect(actual).toEqual(MESSAGE);
    });
});

describe('getAdminMessages getter', () => {
    const adminMessages = [ ADMIN_MESSAGE ];
    const state = {
        adminMessages,
    };

    it('should return state.adminMessages', () => {
        const actual = getters.getAdminMessages(state);

        expect(actual).toEqual(adminMessages);
    });
});
