import { shallowMount } from '@vue/test-utils';
import { mockFabric } from '@unit-tests/fabrics';
import { HomeRoute } from '@router/main-page-routes/home.route';

import Drawer from '@layouts/drawer/Drawer.vue';
import ChatLayout from '@layouts/chat-layout/ChatLayout.vue';
import Header from '@layouts/header/Header.vue';

describe('ChatLayout.vue', () => {
    const mountFunction = options => {
        return shallowMount(ChatLayout, {
            computed: {
                getRoomMembers: () => [],
                getUserId: () => 'userId',
                getUserEmail: () => 'userEmail',
            },
            stubs: [ 'router-view' ],
            ...options,
        });
    };

    it('should mount Drawer component', () => {
        const wrapper = mountFunction();

        expect(wrapper.findComponent(Drawer).exists()).toBeTruthy();
    });

    it('should mount Header component', () => {
        const wrapper = mountFunction();

        expect(wrapper.findComponent(Header).exists()).toBeTruthy();
    });

    it('should close drawer if it opened on Header click', async () => {
        const wrapper = mountFunction();

        await wrapper.setData({ drawer: true });
        wrapper.findComponent(Header).vm.$emit('click');

        expect(wrapper.vm.$data.drawer).toBeFalsy();
    });

    it('should emit socket event leaveFromChat and make leaving from chat', async () => {
        const { mockStore, mockRouter, mockSocket, mockRoute } = mockFabric();
        const wrapper = mountFunction({
            mocks: {
                $store: mockStore,
                $router: mockRouter,
                $socket: mockSocket,
                $route: mockRoute,
            },
        });

        await wrapper.find('.leave-chat').trigger('click');

        expect(mockSocket.emit).toHaveBeenCalledWith('leaveFromChat', {
            roomId: mockRoute.params.id,
            userEmail: wrapper.vm.getUserEmail,
        });
        expect(mockStore.dispatch).toHaveBeenCalledWith('removeMemberFromRoom', {
            roomId: mockRoute.params.id,
            userId: wrapper.vm.getUserId,
        });
        expect(mockRouter.push).toHaveBeenCalledWith(HomeRoute);
    });

    it('push to HomeRoute on exit chat click', async () => {
        const { mockRouter } = mockFabric();
        const wrapper = mountFunction({
            mocks: {
                $router: mockRouter,
            },
        });

        await wrapper.find('.exit-chat').vm.$emit('click');

        expect(mockRouter.push).toHaveBeenCalledWith(HomeRoute);
    });
});
