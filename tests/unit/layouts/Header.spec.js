import Header from '@layouts/header/Header.vue';
import { createWrapperVuetify } from '@unit-tests/fabrics';

describe('Header.vue', () => {
    const headerLabel = 'headerLabel';
    const defaultHeaderLabel = 'Vue chat';
    const mountFunction = options => {
        return createWrapperVuetify(Header, {
            ...options,
        });
    };

    it('should use title from props.headerLabel to toolbar if it passed', () => {
        const wrapper = mountFunction({
            propsData: {
                headerLabel,
            },
        });

        expect(wrapper.find('.v-toolbar__title').text()).toBe(headerLabel);
    });

    it('should use default title from props.headerLabel to toolbar if it not passed', () => {
        const wrapper = mountFunction();

        expect(wrapper.find('.v-toolbar__title').text()).toBe(defaultHeaderLabel);
    });
});
