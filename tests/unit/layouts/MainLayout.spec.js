import { shallowMount } from '@vue/test-utils';
import MainLayout from '@layouts/main-layout/MainLayout.vue';

describe('MainLayout.vue', () => {
    const mountFunction = options => {
        return shallowMount(MainLayout, {
            computed: {
                getIsLoggedIn: () => true,
            },
            stubs: [ 'router-view' ],
            ...options,
        });
    };

    it('show logged layout if user is logged(getIsLoggedIn === true)', () => {
        const wrapper = mountFunction();
        const notLoggedBtn = wrapper.find('.drawer-btn--not-logged');
        const loggedBtn = wrapper.find('.drawer-btn--logged');

        expect(loggedBtn.exists()).toBeTruthy();
        expect(notLoggedBtn.exists()).toBeFalsy();
    });

    it('show not logged layout if user is not logged(getIsLoggedIn === false)', () => {
        const wrapper = mountFunction({
            computed: {
                getIsLoggedIn: () => false,
            },
        });
        const notLoggedBtn = wrapper.find('.drawer-btn--not-logged');
        const loggedBtn = wrapper.find('.drawer-btn--logged');

        expect(notLoggedBtn.exists()).toBeTruthy();
        expect(loggedBtn.exists()).toBeFalsy();
    });
});
