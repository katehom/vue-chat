module.exports = {
    preset: '@vue/cli-plugin-unit-jest',
    setupFiles: [ './tests/setup.js' ],
    transform: {
        '^.+\\.vue$': 'vue-jest',
        '^.+\\.js$': 'babel-jest',
    },
    moduleNameMapper: {
        '^@src(.*)$': '<rootDir>/src$1',
        '^@components(.*)$': '<rootDir>/src/components$1',
        '^@layouts(.*)$': '<rootDir>/src/layouts$1',
        '^@pages(.*)$': '<rootDir>/src/pages$1',
        '^@router(.*)$': '<rootDir>/src/router$1',
        '^@store(.*)$': '<rootDir>/src/store$1',
        '^@unit-tests(.*)$': '<rootDir>/tests/unit$1',
    },
    moduleFileExtensions: [
        'vue',
        'js',
    ],
};
